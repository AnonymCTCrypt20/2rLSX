/*
 * common_headers.h
 *
 */

#ifndef COMMON_STD_HEADERS_H_
#define COMMON_STD_HEADERS_H_

// I/O
#include <iostream>
#include <iomanip>
#include <fstream>

// common
#include <cstdint>
#include <cstdlib>
#include <memory>
#include <string>
#include <cstring>
#include <limits>

// math
#include <cmath>

// alg & containers
#include <algorithm>
#include <vector>
#include <array>
#include <map>
#include <unordered_map>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <bitset>

using namespace std;

#endif /* COMMON_STD_HEADERS_H_ */

/*
 * hamming_weight.h
 *
 */

#ifndef COMMON_HAMMING_WEIGHT_H_
#define COMMON_HAMMING_WEIGHT_H_

#include <cstdint>
#include <vector>

using namespace std;

namespace hw
{
	int compute_hw64(uint64_t x);
	int compute_hw8(uint8_t x);
	void init_byte_hw_table();

	template<typename I>
	size_t vector_hw(const vector<I> &v)
	{
		size_t w = 0;
		for (auto i: v)
			if (i)
				w++;
		return w;
	}
}

#endif /* COMMON_HAMMING_WEIGHT_H_ */

/*
 * arrays.h
 */

#ifndef COMMON_ARRAYS_H_
#define COMMON_ARRAYS_H_

#include "../common/std_headers.h"

template<typename T>
map<T, size_t> counter(const vector<T> &vec)
{
	map<T, size_t> c;
	for (const T& t: vec)
		c[t]++;
	return c;
}

template <typename Arr, typename T = size_t>
vector<T> to_vector(const Arr &arr, size_t size)
{
	vector<T> vec(size);
	for (size_t i = 0; i < size; i++)
		vec[i] = arr[i];
	return vec;
}

template<typename P, typename T>
vector<uint8_t> create_block(const vector<P> &positions, const vector<T> &solution, size_t block_size)
{
	vector<uint8_t> block(block_size, 0);
	for (size_t i = 0; i < positions.size(); i++)
	{
		block[positions[i]] = solution[i];
	}
	return block;
}

template<typename T>
using Vector2D = vector<vector<T>>;

// - const & non-const access(x, y)
// - transpose
// - print
template<typename T>
class Matrix
{

		vector<vector<T>> data_;
	protected:
		size_t row_count_ = 0, column_count_ = 0;

		size_t get_index(const size_t row, const size_t column) const
		{
			return row*column_count_ + column;
		}

		void del_last_rows(size_t count)
		{
			row_count_ -= count;
			data_.resize(row_count_);
		}

	public:

		using Type = T;
		using Ref = typename vector<T>::reference;
		using Cref = typename vector<T>::const_reference;

		Matrix(){}

		Matrix(size_t size): Matrix(size, size){row_count_ = size; column_count_ = size;}

		Matrix(size_t row, size_t column):data_(row, vector<T>(column, T())), row_count_(row), column_count_(column)
		{
		}

		Matrix(const vector<vector<T>> &vectors)
		{
			if (vectors.size() == 0) return;

			row_count_ = vectors.size();
			column_count_ = vectors.front().size();
			//data_.resize(row_count_ * column_count_);
			data_.resize(row_count_, vector<T>(column_count_, T()));
			for (size_t i = 0; i < row_count_; i++)
				for (size_t j = 0; j < column_count_; j++)
					elem(i,j) = vectors[i][j];
		}


		Ref elem(const size_t row, const size_t column)
		{
			return data_[row][column];
		}

		Ref operator()(const size_t row, const size_t column)
		{
			return elem(row, column);
		}

		Cref elem(const size_t row, const size_t column) const
		{
			return data_[row][column];
		}

		Cref operator()(const size_t row, const size_t column) const
		{
			return elem(row, column);
		}

		void transpose()
		{
			if (row() != col()) return;
			for (size_t i = 0; i < row(); i++)
				for (size_t j = i+1; j < col(); j++)
					swap(data_[i][j], data_[j][i]);
		}


		size_t size() const
		{
			return row();
		}

		size_t col() const
		{
			return column_count_;
		}

		size_t row() const
		{
			return row_count_;
		}

		void swap_rows(size_t r1, size_t r2)
		{
			swap(data_[r1], data_[r2]);
		}

		const vector<T>& get_row(size_t row_id) const
		{
			return data_[row_id];
		}

		string to_string() const
		{
			stringstream out;
			for (size_t i = 0; i < row_count_; i++)
			{
				for (size_t j = 0; j < column_count_; j++)
					out << elem(i, j) << " ";
				out << endl;
			}
			return out.str();
		}

		auto get_data() const
		{
			return data_;
		}

		Matrix<T> get_submatrix(const vector<size_t> &row_coords, const vector<size_t> &col_coords)
		{
			Matrix<T> submatrix(row_coords.size(), col_coords.size());

			for (size_t i = 0; i < row_coords.size(); i++)
			{
				for (size_t j = 0; j < col_coords.size(); j++)
				{
					submatrix(i,j) = elem(row_coords[i], col_coords[j]);
				}
			}

			return submatrix;
		}
};

class GF2Matrix: public Matrix<bool>
{

	public:
		GF2Matrix(size_t row, size_t column):Matrix(row, column){}

		GF2Matrix(const vector<vector<bool>> &vectors):Matrix(vectors){}


		vector<bool> mul(const vector<bool> &vec) const
		{
			if (vec.size() != row())
			{
				cerr << "vec.size != row" << endl;
				return vector<bool>();
			}

			vector<bool> res(col(), 0);
			for (size_t i = 0; i < col(); i++)
			{
				for (size_t j = 0; j < row(); j++)
					res[i] = res[i] ^ (vec[j] & elem(j,i));
			}

			return res;
		}

};


#endif /* COMMON_ARRAYS_H_ */

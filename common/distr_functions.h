/*
 * distr_functions.h
 *
 */

#ifndef COMMON_DISTR_FUNCTIONS_H_
#define COMMON_DISTR_FUNCTIONS_H_

#include "std_headers.h"

template<typename Int>
map<double, Int> direct_product_pow_distr(const map<double, Int> &distr, size_t prod_pow)
{
	if (prod_pow == 1)
		return distr;

	map<double, Int> res = distr;

	for (size_t i = 1; i <= prod_pow - 1; i++)
	{
		map<double, Int> temp;
		for (auto p1 : distr)
		{
			for (auto p2 : res)
			{
				temp[p1.first * p2.first] += p1.second * p2.second;
			}
		}
		res = temp;
	}
	return res;
}

template<typename Int>
map<double, Int> scalar_mul_distr(map<double, Int> x_distr, map<double, Int> y_distr)
{
	map<double, Int> result;

	auto x_it = x_distr.rbegin();
	auto y_it = y_distr.rbegin();

	while (x_it != x_distr.rend() && y_it != y_distr.rend())
	{
		double x_val = x_it->first, y_val = y_it->first;
		Int &x_count = x_it->second, &y_count = y_it->second;

		if (x_count == y_count)
		{
			result[x_val * y_val] = x_count;
			x_count = y_count = 0;
			x_it++;
			y_it++;
		}
		else
			if (x_count > y_count)
			{
				result[x_val * y_val] = y_count;
				x_count -= y_count;
				y_count = 0;
				y_it++;
			}
			else //x_count < y_count
			{
				result[x_val * y_val] = x_count;
				y_count -= x_count;
				x_count = 0;
				x_it++;
			}
	}
	return result;
}

template<typename Int>
map<double, Int> trunc_distr(const map<double, Int> &distr)
{
	map<double, Int> t_distr;
	double sum_pr = 0.0;

	for (auto it = distr.rbegin(); it != distr.rend(); it++)
	{
		double pr = it->first;
		Int c  = it->second;

		if ((sum_pr + pr*c) >= 1)
		{
			c = floor((1-sum_pr)/pr);
			t_distr[pr] += c;
			double rem_pr = 1 - (sum_pr + pr * c);
			if (rem_pr > 0)
				t_distr[rem_pr] = 1;

			return t_distr;
		}
		else
		{
			t_distr[pr] += c;
		}
		sum_pr += pr*c;
	}
	return t_distr;
}

template<typename Int>
double sum_distr(const map<double, Int> &distr)
{
	double sum_pr = 0.0;
	for (auto &elem: distr)
	{
		sum_pr += elem.first * elem.second;
	}
	return sum_pr;
}

#endif /* COMMON_DISTR_FUNCTIONS_H_ */

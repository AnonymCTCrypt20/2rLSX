/*
 * bit_functions.cpp
 *
 */

#include "bit_functions.h"

uint64_t bin_to_uint(const vector<bool> &vec)
{
	uint64_t x = 0;
	for (size_t i = 0; i < vec.size(); i++)
	{
		x ^= uint64_t(vec[i]) << (vec.size()-i-1);
	}
	return x;
}



/*
 * factorials.h
 *
 */

#ifndef COMMON_FACTORIALS_H_
#define COMMON_FACTORIALS_H_

#include "../common/std_headers.h"

size_t factorial(size_t n)
{
	const size_t max_n = 20;
	if (n > max_n)
	{
		cerr << "factorial(" << n << ")" << " = inf!" << endl;
		return 0;
	}
	size_t result = 1;
	for (size_t i = 2; i <= n; i++)
		result *= i;
	return result;
}



template<typename Uint>
Uint number_of_combinations(size_t n, size_t k)
{
	if (k > n) return 0;
	Uint result = 1;
	size_t t = 2;
	for (size_t i = n-k+1; i <= n; i++)
	{
		result *= i;
		while (t <= k && (result%t == 0))
		{
			result /= t;
			t++;
		}
	}
	return result;
}

size_t number_of_combinations(size_t n, size_t k)
{
	return number_of_combinations<size_t>(n, k);
}
#endif /* COMMON_FACTORIALS_H_ */

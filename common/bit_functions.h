/*
 * bit_functions.h
 *
 */

#ifndef COMMON_BIT_FUNCTIONS_H_
#define COMMON_BIT_FUNCTIONS_H_

#include "../common/std_headers.h"

template<typename T>
bool is_pow2(T x)
{
	if (!x) return false;
	return !(x & (x-1));
}

inline uint64_t pow2(uint32_t k)
{
	//if k == 0 return ERROR
	return uint64_t(1) << (k);
}

inline uint64_t mask(uint32_t n)
{
	if (n == 64) return ~uint64_t(0);
	return pow2(n) - 1;
}

template<typename T>
inline bool get_bit(T x, uint pos)
{
	return x & pow2(pos);
}

template<typename T>
inline void set_zero_bit(T &x, uint pos)
{
	x &= ~pow2(pos);
}

template<typename T>
inline void set_one_bit(T &x, uint pos)
{
	x |=  pow2(pos);
}

template<typename T>
vector<size_t> get_bit_positions(T x)
{
	vector<size_t> positions;
	size_t pos = 0;
	while (x)
	{
		if (x & 1)
			positions.push_back(pos);
		pos++;
		x >>= 1;
	}
	return positions;
}

template<typename T>
vector<uint8_t> get_bytes(T x)
{
	const size_t bytes_count = sizeof(T);
	vector<uint8_t> bytes(bytes_count, 0x00);
	T* ptr = reinterpret_cast<T*>(bytes.data());
	*ptr = x;
	return bytes;
}

template<typename T>
T get_word(const vector<uint8_t> &bytes)
{
	T* ptr = reinterpret_cast<T*>(const_cast<uint8_t*>(bytes.data()));
	return *ptr;
}


template<typename T>
vector<bool> uint_to_bin(T x, const size_t bit_count = sizeof(T)*8)
{
	vector<bool> vec(bit_count, false);
	uint64_t mask = 1;
	mask <<= bit_count-1;
	for (size_t i = 0; i < bit_count; i++)
	{
		vec[i] = bool(x & mask);
		x <<= 1;
	}
	return vec;
}

uint64_t bin_to_uint(const vector<bool> &vec);

#endif /* COMMON_BIT_FUNCTIONS_H_ */

/*
 * differential.h
 *
 */

#ifndef COMMON_DIFFERENTIAL_H_
#define COMMON_DIFFERENTIAL_H_

#include "../common/std_headers.h"
#include "../common/arrays.h"
#include "../common/bit_functions.h"

using DDT = Matrix<size_t>;

bool is_pow2(size_t n)
{
   return n && !(n & (n-1));
}


size_t get_sbox_bit_count(size_t sbox_size)
{
	if (!is_pow2(sbox_size))
		return 0; // or throw exception
	return log2(sbox_size);
}

template<typename Arr>
size_t get_sbox_bit_count(const Arr &sbox)
{
	return get_sbox_bit_count(sbox.size());
}

template<typename Arr>
bool is_bijective(const Arr &sbox)
{
	const size_t sbox_size = sbox.size();
	vector<bool> numbers_set(sbox_size, false);
	for (auto i: sbox)
	{
		if (i >= sbox_size) return false;
		numbers_set[i] = true;
	}

	for (bool b: numbers_set)
		if (b == false)
			return false;

	return true;
}

template<typename Arr>
DDT compute_ddt(const Arr &sbox, bool debug_print = false)
{
	const size_t sbox_size = sbox.size();
	DDT ddt(sbox_size, sbox_size);
	for (size_t a = 0; a < sbox_size; a++)
	{
		if (debug_print)
			cout << "a = " << a << endl;
		for (size_t dx = 0; dx < sbox_size; dx++)
		{
			size_t dy = sbox[a] ^ sbox[a ^ dx];
			ddt(dx,dy)++;
		}
	}
	return ddt;
}

template<typename Arr>
Matrix<double> compute_norm_ddt(const Arr &sbox)
{
	const size_t sbox_size = sbox.size();
	DDT ddt = compute_ddt(sbox);
	Matrix<double> norm_ddt(sbox_size, sbox_size);

	for (size_t i = 0; i < sbox_size; i++)
	{
		for (size_t j = 0; j < sbox_size; j++)
		{
			norm_ddt(i,j) = ddt(i,j)/double(sbox_size);
		}
	}
	return norm_ddt;
}


map<size_t, size_t> compute_ddt_distr(const DDT &ddt)
{
	const size_t sbox_size = ddt.row();
	map<size_t, size_t> distr;
	for (size_t dx = 0; dx < sbox_size; dx++)
	{
		for (size_t dy = 0; dy < sbox_size; dy++)
		{
			distr[ddt(dx,dy)]++;
		}
	}
	return distr;
}

template<typename T>
double get_log2_max_pr(const vector<T> &sbox)
{
	auto sbox_distr = compute_ddt_distr(sbox);

	size_t sbox_bit_size = get_sbox_bit_count(sbox);
	double max_sbox_val = (--sbox_distr.rbegin())->first;
	double local_pr = log2(max_sbox_val / pow2(sbox_bit_size));

	return local_pr;
}

template<typename Table>
auto compute_max_table_pr(Table table, bool inverse = false)
{
	if (inverse)
	{
		table.transpose();
	}
	const size_t size = table.size();
	vector<typename Table::Type> max(size, 0);
	for (uint dx = 0; dx < size; dx++)
		for (uint dy = 0; dy < size; dy++)
		{
			auto elem = table(dx,dy);
			if (elem > max[dx])
				max[dx] = elem;
		}
	return max;
}

template<typename T>
auto compute_max_in_out_diff(const vector<T> &sbox)
{
	auto ddt = compute_ddt(sbox);
	return compute_max_table_pr(ddt);
}

template<typename T>
auto compute_max_out_in_diff(const vector<T> &sbox)
{
	auto ddt = compute_ddt(sbox);
	return compute_max_table_pr(ddt, true);
}

size_t get_ddt_max_val(const DDT &ddt)
{
	size_t max_val = 0;
	for (size_t i = 1; i < ddt.row(); i++)
	{
		for (size_t j = 1; j < ddt.col(); j++)
		{
			if (ddt(i,j) > max_val)
				max_val = ddt(i,j);
		}
	}
	return max_val;
}

template<typename T>
size_t get_ddt_max_val(const vector<T> &sbox)
{
	DDT ddt = compute_ddt(sbox);
	return get_ddt_max_val(ddt);
}

void check_overflow(size_t max_val, size_t max_pow)
{
	const size_t max_bit_count = 63;
	if (log2(max_val) * max_pow > max_bit_count)
		cerr << "Attention: overflow is possible!" << endl;
}

double numerator_to_fraction(double numerator, size_t trail_weight, const size_t values_count = 256)
{
	return numerator/pow(values_count, trail_weight);
}

double log2_pr(double numerator, size_t trail_weight)
{
	if (numerator > 0)
		return std::log2(numerator_to_fraction(numerator, trail_weight));
	else
		return NAN;
}

double get_universal_est(size_t max_val, size_t trail_weight)
{
	return log2_pr(pow(max_val, trail_weight), trail_weight);
}

template<typename T>
vector<vector<T>> create_possible_diff_list(const vector<T> &sbox, bool in_to_out = true, size_t min_val = 0)
{
	DDT ddt = compute_ddt(sbox);
	vector<vector<T>> lists(sbox.size());
	for (size_t dx = 0; dx < sbox.size(); dx++)
	{
		for (size_t dy = 0; dy < sbox.size(); dy++)
		{
			if (ddt(dx,dy) > min_val)
			{
				if (in_to_out)
					lists[dx].push_back(dy);
				else
					lists[dy].push_back(dx);
			}
		}
	}
	return lists;
}

template<typename T>
vector<T> create_major_distr(Matrix<T> table, bool inverse = false)
{
	if (inverse)
	{
		table.transpose();
	}

	auto data = table.get_data();

	for (auto &row: data)
		sort(row.begin(), row.end(), greater<T>());

	vector<T> major;
	for (size_t i = 0; i < table.size(); i++)
	{
		T max = 0;
		for (size_t j = 1; j < table.size(); j++)
			if (data[j][i] > max)
				max = data[j][i];
		if (max)
			major.push_back(max);
		else
			break;
	}

	return major;
}

template<typename T>
vector<uint8_t> create_major_sbox_distr(const vector<T> &sbox, bool inverse = false)
{
	auto ddt = compute_ddt(sbox);
	auto major = create_major_distr(ddt, inverse);
	return vector<uint8_t>(major.begin(), major.end());

}

template<typename Table>
auto compute_max_elem_lists(Table table, bool inverse = false)
{
	if (inverse)
	{
		table.transpose();
	}

	vector<vector<typename Table::Type>> max_list_(table.row());
	for (size_t i = 0; i < table.row(); i++)
	{
		auto max_val = table(i,0);
		for (size_t j = 0; j < table.col(); j++)
			if (table(i,j) > max_val) max_val = table(i,j);
		for (size_t j = 0; j < table.col(); j++)
		{
			if (table(i,j) == max_val)
				max_list_[i].push_back(j);
		}
	}
	return max_list_;
}

template<typename T>
auto compute_max_diff_in_out_lists(const vector<T> &sbox)
{
	auto ddt = compute_ddt(sbox);
	return compute_max_elem_lists(ddt);
}


template<typename T>
auto compute_max_diff_out_in_lists(const vector<T> &sbox)
{
	auto ddt = compute_ddt(sbox);
	return compute_max_elem_lists(ddt, true);
}

#endif /* COMMON_DIFFERENTIAL_H_ */

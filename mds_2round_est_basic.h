/*
 * mds_2round_est.h
 *
 */

#ifndef LSX_DIFF_TRAIL_MDS_2ROUND_EST_H_
#define LSX_DIFF_TRAIL_MDS_2ROUND_EST_H_

#include "common/std_headers.h"
#include "common/arrays.h"
#include "common/factorials.h"
#include "common/distr_functions.h"

class MDS2RoundEstBasic
{
	protected:
		vector<double> major_x_;
		vector<double> major_y_;

		map<double, size_t> major_x_map_;
		map<double, size_t> major_y_map_;

		vector<double> max_row_x_;
		vector<double> max_row_y_;

		double p_max_ = 0.0;
		size_t p_max_count_ = 0;
		size_t block_byte_size_ = 0;
		size_t code_distance_ = 0;
		size_t active_sbox_count_ = 0;
		size_t major_size_ = 0;

		void init(Matrix<double> &table, size_t code_distance, size_t active_sbox_count)
		{
			code_distance_ = code_distance;
			active_sbox_count_ = active_sbox_count;
			block_byte_size_ = code_distance_ - 1;

			major_x_ = get_major_x(table);
			major_y_ = get_major_y(table);

			major_size_= min(major_x_.size(), major_y_.size());
			major_x_.resize(major_size_);
			major_y_.resize(major_size_);

			//if major_x > major_y:
			//		swap(major_x, major_y);
			//		swap(max_row_x, max_row_y);

			major_x_map_ = counter(major_x_);
			major_y_map_ = counter(major_y_);

			p_max_ = get_max_table_val(table);
			p_max_count_ = major_y_map_.at(p_max_);

			max_row_x_ = get_max_row_x_(table);
			max_row_y_ = get_max_row_y_(table);
		}

		vector<double> get_major(Matrix<double> table) const
		{
			vector<vector<double>> tab = table.get_data();

			for (auto &row: tab)
				sort(row.begin(), row.end(), greater<double>());

			vector<double> major;
			for (size_t i = 0; i < tab.size(); i++)
			{
				double max = 0;
				for (size_t j = 1; j < tab[i].size(); j++)
					if (tab[j][i] > max)
						max = tab[j][i];
				if (max == 0)
					break;
				major.push_back(max);
			}

			return major;
		}

		vector<double> get_max_row(Matrix<double> table) const
		{
			vector<vector<double>> tab = table.get_data();

			for (auto &row: tab)
				sort(row.begin(), row.end(), greater<double>());

			sort(tab.begin(), tab.end(), greater<vector<double>>());

			size_t nonzero_count = 0;
			while (tab[1][nonzero_count] > 0.0)
				nonzero_count++;

			return vector<double>(tab[1].begin(), tab[1].begin() + nonzero_count);
		}

		vector<double> get_major_x(Matrix<double> table) const
		{
			return get_major(table);
		}

		vector<double> get_major_y(Matrix<double> table) const
		{
			table.transpose();
			return get_major(table);
		}

		vector<double> get_max_row_x_(Matrix<double> table)
		{
			return get_max_row(table);
		}

		vector<double> get_max_row_y_(Matrix<double> table)
		{
			table.transpose();
			return get_max_row(table);
		}

		double get_max_table_val(const Matrix<double> &table) const
		{
			double max = 0;
			for (size_t i = 1; i < table.size(); i++)
			{
				for (size_t j = 1; j < table.size(); j++)
				{
					if (table(i,j) > max)
						max = table(i,j);
				}
			}
			return max;
		}
};


class MDS2RoundEst: public MDS2RoundEstBasic
{
	public:

		double get_est_on_min_distance() const
		{
			double est = 0.0;
			for (size_t i = 0; i < major_size_; i++)
			{
				double trail_pr = major_x_[i]*pow(major_y_[i], code_distance_-1);
				est += trail_pr;
			}
			return est;
		}

		vector<size_t> max_val_limits_;
		vector<size_t> pair_weights_;
		size_t total_pairs_count_ = 0;
		size_t total_max_val_count = 0;
		double best_est_ = 0.0;

		void construct_all_max_val_spectr()
		{
			size_t columns_count = code_distance_ - 1;
			vector<size_t> spectr(columns_count + 1, 0);
			max_val_limits_ = get_max_val_limits();
			pair_weights_.resize(columns_count + 1);
			for (size_t i = 2; i <= columns_count; i++)
				pair_weights_[i] = number_of_combinations(i,2);

			total_pairs_count_ = number_of_combinations(columns_count,2) * pow(major_y_map_.at(p_max_), 2);
			total_max_val_count = major_y_map_.at(p_max_) * columns_count * major_size_;
			best_est_ = 0.0;

			size_t top_weight_row_count = 5;
			vector<size_t> top_weights;
			topN_weight_construct(top_weight_row_count, columns_count, top_weights);
			cout << "spectr_count = " << spectr_count << endl;
			//recursive_construct_spectr(columns_count, total_pairs_count_, total_max_val_count,  0,  spectr);
		}


		size_t get_pair_count(const vector<size_t> &weights) const
		{
			size_t pairs_count = 0;

			for (size_t w: weights)
				pairs_count += pair_weights_[w];

			return pairs_count;
		}

		bool topN_weight_filter(vector<size_t> &weights) const
		{
			for (size_t i = 0; i < weights.size(); i++)
			{
				size_t w = weights[i];
				size_t pos = i+1;
				if (max_val_limits_[w] < pos)
					return false;
			}

			size_t w1 = weights[0], w2 = weights[1], w3 = weights[2];

			size_t columns_count = code_distance_ - 1;
			size_t max_val_in_column = major_y_map_.at(p_max_);

			if ((max_val_in_column*columns_count - w1 - w2 + 2) < w3)
				return false;

			return true;
		}

		void topN_weight_construct(size_t N, size_t weight, vector<size_t> &weights)
		{
			if (N == 0)
			{
				if (topN_weight_filter(weights))
				{
					vector<size_t> spectr(code_distance_, 0);
					size_t min_weight = max(size_t(2), weights.back());
					size_t pair_count = 0;
					size_t total_rows = 0;
					size_t max_val_count = 0;
					for (size_t w: weights)
					{
						if (w == min_weight)
							break;

						spectr[w]++;
						pair_count += pair_weights_[w];
						max_val_count += w;
						total_rows++;

					}
					recursive_construct_spectr(min_weight, total_pairs_count_ - pair_count, total_max_val_count - max_val_count, total_rows, spectr);

				}
				return;
			}
		    for (size_t w = weight; w >= 1; w--)
		    {
		        weights.push_back(w);
				topN_weight_construct(N-1, w, weights);
		        weights.pop_back();
		    }
		}

		size_t spectr_count = 0;

		void recursive_construct_spectr(size_t weight, size_t rem_pair_count, size_t rem_max_val_count,  size_t total_rows, vector<size_t> &spectr)
		{
			if (weight <= 2)
			{
				spectr[2] = rem_pair_count;
				spectr[1] = rem_max_val_count - rem_pair_count*2;
				spectr[0] = major_size_ * major_size_ - total_rows - spectr[1] - spectr[2];
				spectr_count++;

				auto y_rows = construct_spectr_of_rows(spectr, elements_distr_);

				//size_t x_rows_count = get_rows_count(major_x2_), y_rows_count = get_rows_count(y_rows);

				auto mul_x_y = scalar_mul_distr(major_x2_, y_rows);

				double est = compute_spectr_est(mul_x_y);

				if (est > best_est_)
				{
					best_est_ = est;
					for (size_t w: spectr) cout << w << " ";

					cout << " !!! " << log2(est) << endl; cin.get();
				}

				if (spectr_count % 1000 == 0)
				{
					for (size_t w: spectr) cout << w << " ";

					cout << " --- " << log2(est) << " " << spectr_count << endl;
				}

				return;
			}

			size_t max_rows_count1 = max_val_limits_[weight] - total_rows;

			size_t max_rows_count2 = rem_pair_count/pair_weights_[weight];

			if (max_rows_count2 < max_rows_count1)
				max_rows_count1 = max_rows_count2;

			for (size_t rows_count = 0; rows_count <= max_rows_count1; rows_count++)
			{
				spectr[weight] = rows_count;

				recursive_construct_spectr(weight-1, rem_pair_count - rows_count*pair_weights_[weight], rem_max_val_count - rows_count*weight, total_rows + rows_count, spectr);
			}
		}

		map<double, size_t> major_x2_;
		map<double, size_t> elements_distr_;

		double get_est_on_min_d_plus_1()
		{
			major_x2_ = direct_product_pow_distr(major_x_map_, 2);

			auto max_val_limits = get_nested_max_val_limits();

			for (auto i: max_val_limits)
				cout << i << " ";
			cout << endl;

			elements_distr_ = get_non_max_elem_distr();

			construct_all_max_val_spectr();

			return best_est_;
		}


		double compute_spectr_est(const map<double, size_t> &spectr) const
		{
			double est = 0.0;
			for (auto elem: spectr)
			{
				est += elem.first * elem.second;
			}
			return est;
		}


		map<double,size_t> construct_spectr_of_rows(vector<size_t> max_val_spectr, map<double, size_t> elem_distr) const
		{
			map<double,size_t> result;
			const size_t columns_count = code_distance_ - 1;

			auto elem_it = --elem_distr.end();
			for (int i = max_val_spectr.size()-1; i >= 0; i--)
			{
				size_t max_val_rows = max_val_spectr[i];
				size_t max_val_cols = i;
				size_t elems_cols = (columns_count - max_val_cols);
				size_t req_elems_count = elems_cols * max_val_rows;

				//cout << max_val_rows << " " << max_val_cols << endl;

				do
				{
					double elem_val = elem_it->first;
					size_t &elem_freq = elem_it->second;

					double row_est = pow(p_max_, max_val_cols) * pow(elem_val, elems_cols);
					size_t row_count = 0;

					if (elem_freq >= req_elems_count)
					{
						elem_freq -= req_elems_count;
						req_elems_count = 0;

						row_count = max_val_rows;

					}
					else
					{
						row_count = elem_freq/elems_cols + 1;
						max_val_rows -= row_count;
						req_elems_count -= row_count*elems_cols;

						elem_freq = 0;
						elem_it--;
					}

					if (row_count)
						result[row_est] += row_count;
				}
				while (req_elems_count != 0);
			}

			return result;
		}

		vector<size_t> get_max_val_limits() const
		{
			size_t block_size = active_sbox_count_ - code_distance_ + 1;
			size_t n = code_distance_-1;
			size_t max_val_in_col = major_y_map_.at(p_max_);
			size_t block_count = number_of_combinations(n,block_size) * pow(max_val_in_col, block_size);

			cout << block_count << endl;

			size_t max_val_count = pow(major_size_, block_size-1) * n * max_val_in_col;

			vector<size_t> limits(n+1,0);

			limits[0] = pow(major_x_.size(), block_size);



			limits[block_size - 1] =  max_val_count/(block_size - 1); 

			for (size_t i = block_size; i <= n; i++)
			{
				limits[i] = block_count / number_of_combinations(i, block_size);

				cout << block_count << " " << number_of_combinations(i, block_size) << " " << limits[i] << endl;

			}

			size_t max_val_bytes_count = n*max_val_in_col;

			for (size_t i = n; i >= block_size; i--)
			{
				double row_cost = i;

				double s = block_size - 1;

				double b = -(row_cost + s/2);

				double D = b*b - 2 * s * max_val_bytes_count;

				if (D <= 0) break;

				double x1 = (-b - sqrt(D))/s;

				limits[i] = floor(x1);
			}

			return limits;
		}


		map<double, size_t> get_non_max_elem_distr() const
		{
			size_t y_columns_count = code_distance_ - 1;
			size_t x_column_count = active_sbox_count_ - code_distance_ + 1;
			size_t x_blocks_count = pow(major_x_.size(), x_column_count-1);

			map<double, size_t> elements_distr;

			for (auto p: major_y_map_)
			{
				double val = p.first;
				size_t freq = p.second;
				if (p.first != p_max_)
				{
					elements_distr[val] = freq*y_columns_count*x_blocks_count;
				}
			}

			return elements_distr;
		}

		vector<size_t> get_nested_max_val_limits() const
		{
			auto limits = get_max_val_limits();
			size_t n = code_distance_-1;
			for (int i = n; i >= 1; i--)
			{
				for (int j = i-1; j >= 0; j--)
					if (limits[j] > limits[i])
						limits[j] -= limits[i];
					else
						limits[j] = 0;
			}
			return limits;
		}


		double get_est_on_active_sbox()
		{
			size_t x_column_count = active_sbox_count_ - code_distance_ + 1;

			auto major_x_t = direct_product_pow_distr(major_x_map_, x_column_count);

			auto max_val_limits = get_nested_max_val_limits();

			max_val_limits.back() = 0;

			for (auto i: max_val_limits)
				cout << i << " ";
			cout << endl;
			map<double, size_t> elements_distr = get_non_max_elem_distr();


			auto y_rows = construct_spectr_of_rows(max_val_limits, elements_distr);

			size_t x_rows_count = get_rows_count(major_x_t), y_rows_count = get_rows_count(y_rows);

			cout << x_rows_count << " " << y_rows_count << endl;

			auto mul_x_y = scalar_mul_distr(major_x_t, y_rows);

			return compute_spectr_est(mul_x_y);
		}

		size_t get_rows_count(const map<double, size_t> &spectr) const
		{
			size_t counter = 0;
			for (auto p: spectr)
				counter += p.second;
			return counter;
		}





	public:

		MDS2RoundEst()
		{

		}

		double get_est(Matrix<double> &table, size_t code_distance, size_t active_sbox_count)
		{
			MDS2RoundEstBasic::init(table, code_distance, active_sbox_count);

			if ((active_sbox_count - code_distance) > 7)
			{
				cerr << "(active_sbox_count - code_distance) > 7 !!!" << endl;
				return 1.0;
			}
			if (code_distance == active_sbox_count)
				return get_est_on_min_distance();

			if ((code_distance+1) == active_sbox_count)
				return get_est_on_min_d_plus_1();

			if (code_distance < active_sbox_count)
				return get_est_on_active_sbox();


			return 1.0;
		}
};


#endif /* LSX_DIFF_TRAIL_MDS_2ROUND_EST_H_ */

/*
 * mds_2round_dyn_prog_est.h
 *
 */

#ifndef LSX_DIFF_TRAIL_MDS_2ROUND_DYN_PROG_EST_H_
#define LSX_DIFF_TRAIL_MDS_2ROUND_DYN_PROG_EST_H_

#include "common/std_headers.h"
#include "common/arrays.h"
#include "common/factorials.h"
#include "common/print_functions.h"
//#include "common/block_gen.h"

#include "mds_2round_est_basic.h"

class PartialPartitionGenerator
{
		vector<vector<size_t>> partitions_;

		void recursive(size_t t, vector<size_t> &partition, size_t reminder)
		{
			if (reminder == 0)
			{
				partitions_.push_back(partition);
				return;
			}

			if (t == 1)
			{
				partitions_.push_back(partition);
				partitions_.back().resize(partition.size() + reminder, 1);
				return;
			}

			for (size_t i = t; i > 0; i--)
			{
				if (reminder >= i)
				{
					partition.push_back(i);
					recursive(i, partition, reminder - i);
					partition.pop_back();
				}
			}
		}

	public:
		PartialPartitionGenerator()
		{
		}

		vector<vector<size_t>>&& gen(size_t n, size_t max_part)
		{
			vector<size_t> partition;
			partitions_ = vector<vector<size_t>>();
			recursive(max_part, partition, n);
			return move(partitions_);
		}
};

class NonIncreasingSequenceGenerator
{
		vector<vector<size_t>> sequences_;
		size_t max_len_ = 0, min_val_ = 0;


		void recursive(size_t curr_len, size_t val, vector<size_t> &seq)
		{
			if (curr_len == max_len_)
			{
				sequences_.push_back(seq);
				return;
			}

			for (int i = val; i >= int(min_val_); i--)
			{
				seq.push_back(i);
				recursive(curr_len+1, i, seq);
				seq.pop_back();
			}
		}
	public:
		NonIncreasingSequenceGenerator()
		{
		}

		vector<vector<size_t>>&& gen(size_t length, size_t max_value, size_t min_value = 1)
		{
			max_len_ = length;
			min_val_ = min_value;
			vector<size_t> sequence;
			sequences_ = vector<vector<size_t>>();
			recursive(0, max_value, sequence);
			return move(sequences_);
		}
};

class HashVector
{
	public:
		size_t operator()(const vector<size_t> &vec) const
		{
			size_t h = 0;
			for (size_t i = 0; i < vec.size(); i++)
			{
				h += (vec[i] << 3) + (vec[i] >> 1);
			}
			return h;
		}
};

class HashPair
{
	public:
		size_t operator()(const pair<vector<size_t>, size_t> &p) const
		{
			return HashVector()(p.first) + (p.second << 10);
		}
};


class MDS2RoundEstDynProg: public MDS2RoundEstBasic
{
		const size_t min_cost_type_ = 2, max_cost_type_ = 4;

		size_t pairs_count_ = 0, triples_count_ = 0;

		struct Block
		{
				vector<size_t> p_max_distr;
				double pr;

				size_t pair_cost;
				size_t triple_cost;
		};

		vector<Block> blocks_data_;


		double compute_block_pr(const vector<size_t>& p_max_distr) const
		{
			double pr = 0;

			map<double, size_t> elems = major_y_map_;
			elems.erase(p_max_);
			for (pair<const double, size_t> &elem: elems)
				elem.second *= block_byte_size_;

			for (size_t row_id = 0; row_id < major_size_; row_id++)
			{
				size_t w = 0;
				if (row_id < p_max_distr.size())
					w = p_max_distr[row_id];

				double row_pr = pow(p_max_, w);
				size_t rem_columns = block_byte_size_ - w;

				double pr_curr = elems.rbegin()->first;

				while (rem_columns)
				{
					if (elems[pr_curr] >= rem_columns)
					{
						elems[pr_curr] -= rem_columns;
						row_pr *= pow(pr_curr, rem_columns);
						rem_columns = 0;
					}
					else
					{
						size_t elems_pr_curr = elems[pr_curr];
						row_pr *= pow(pr_curr, elems_pr_curr);
						rem_columns -= elems_pr_curr;
						elems.erase(pr_curr);
						pr_curr = elems.rbegin()->first;
					}
				}

				pr += max_row_x_[row_id] * row_pr;

			}

			return pr;
		}

		map<size_t, size_t> compute_block_costs(const vector<size_t> &p_max_distr) const
		{
			map<size_t, size_t> costs;
			for (size_t t = min_cost_type_; t <= max_cost_type_; t++)
			{
				for (size_t c: p_max_distr)
				{
					costs[t] += number_of_combinations(c, t);
				}
			}
			return costs;
		}

		void gen_blocks_data()
		{
            blocks_data_.clear();
			size_t max_elem_per_block = major_y_map_[p_max_] * block_byte_size_;
			auto max_elem_distr = PartialPartitionGenerator().gen(max_elem_per_block, block_byte_size_);
			for (vector<size_t> &p_max_distr: max_elem_distr)
			{
				double pr = compute_block_pr(p_max_distr);
				auto costs = compute_block_costs(p_max_distr);
				size_t pair_cost = costs[2];
				size_t triple_cost = costs[3];
				blocks_data_.push_back({p_max_distr, pr, pair_cost, triple_cost});

			}

			sort(blocks_data_.begin(), blocks_data_.end(), [](const Block &a, const Block &b) {return a.pr > b.pr;});
		}

		double get_est_on_theta() const
		{
			return blocks_data_.front().pr;
		}

		bool check_top_weights_set(const vector<size_t> &weights) const
		{
		    int omega = block_byte_size_ * p_max_count_;
		    for (int i = 0; i < int(weights.size()); i++)
		    {
		    	int w = weights[i];
		        omega -= (w-i);
		        if (w <= i)
		        	return true;
		        if (omega < 0)
		        	return false;
		    }
		    return true;
		}


		unordered_set<vector<size_t>, HashVector>  gen_top_weights_set(const size_t weights_count) const
		{
			unordered_set<vector<size_t>, HashVector> top_weights;

			NonIncreasingSequenceGenerator weights_generator;
			vector<vector<size_t>> all_weights = weights_generator.gen(weights_count, block_byte_size_);

			for (const vector<size_t> &weights: all_weights)
			{
				if (check_top_weights_set(weights))
				{
					top_weights.insert(weights);
					cout << vec_to_str(weights) << endl;
				}
			}

			cout << "|w| = " << top_weights.size() << endl;
			return top_weights;
		}

		double get_est_on_theta_plus_1_with_top_weights() const
		{
			const size_t top_weights_count = 3;
			using WeightsConfig = vector<size_t>;

			unordered_set<WeightsConfig, HashVector> legal_weights = gen_top_weights_set(top_weights_count);
			using Config = pair<WeightsConfig, size_t>;

			vector<double> rev_max_row_dx(max_row_x_.rbegin(), max_row_x_.rend());

			unordered_map<Config, double, HashPair> config_to_est;

			unordered_set<Config, HashPair> all_configs;

			for (const WeightsConfig &w_conf: legal_weights)
			{
				for (size_t cost = 0; cost <= pairs_count_; cost++)
				{
					auto conf = make_pair(w_conf, cost);
					config_to_est[conf] = 0.0;
					all_configs.insert(conf);
				}
			}

			vector<vector<pair<Config, Config>>> multable(blocks_data_.size());
			for (size_t i = 0; i < blocks_data_.size(); i++) 
			{
				const Block &block = blocks_data_[i];
				for (const Config &conf: all_configs) 
				{
					size_t conf_pair_cost = conf.second;
					size_t sum_pair_cost = block.pair_cost + conf_pair_cost;

					if (sum_pair_cost > pairs_count_)
						continue;

					const WeightsConfig &conf_weights = conf.first;
					WeightsConfig sum_top_weights(2*top_weights_count, 0);
					merge(
						block.p_max_distr.begin(), block.p_max_distr.begin() + top_weights_count,
						conf_weights.begin(), conf_weights.end(),
						sum_top_weights.begin(),greater<size_t>());

					sum_top_weights.resize(top_weights_count);


					if (legal_weights.find(sum_top_weights) == legal_weights.end())
						continue;

					Config new_conf = make_pair(sum_top_weights, sum_pair_cost);
					multable[i].push_back(make_pair(conf, new_conf));
				}
				cout << i << " " << multable[i].size() << endl;
			}

			cout << "Blocks = " << blocks_data_.size() << endl;
			cout << "Configs = " << config_to_est.size() << endl;
			size_t m_pr = 0;
			for (double x_pr : rev_max_row_dx)
			{
				unordered_map<Config, double, HashPair> new_config_to_est;
				cout << m_pr++ << " " << x_pr << endl;
				size_t block_progress = 0;
				for (size_t i = 0; i < blocks_data_.size(); i++) //по всем блокам
				{
					cout << block_progress++ << "/" << blocks_data_.size() << endl;

					const Block &block = blocks_data_[i];

					for (auto conf_pair_it = multable[i].begin(); conf_pair_it != multable[i].end(); conf_pair_it++)
					{
						Config &conf = conf_pair_it->first;
						Config &new_conf = conf_pair_it->second;

						double sum_pr = x_pr * block.pr + config_to_est[conf];

						if (new_config_to_est[new_conf] <= sum_pr)
						{
							new_config_to_est[new_conf] = sum_pr;
						}
					}
				}
				config_to_est = new_config_to_est;
			}

			auto best_config_it = config_to_est.begin();

			for (auto it = config_to_est.begin(); it != config_to_est.end(); it++)
			{
				if (it->second > best_config_it->second)
					best_config_it = it;
			}

			cout << "w = " << vec_to_str(best_config_it->first.first) << " c2 = " << best_config_it->first.second << " pr = " << log2(best_config_it->second) << endl;

			return best_config_it->second;
		}

		double get_est_on_theta_plus_1_pairs_method() const
		{
			using Cost = size_t; 

			vector<vector<Block>> history(pairs_count_+1);

			vector<double> rev_max_row_dx(max_row_x_.rbegin(), max_row_x_.rend());

			vector<Block> cost_to_block(pairs_count_+1);
			vector<double> cost_to_est(pairs_count_+1, 0.0);

			for (auto block_it = blocks_data_.rbegin(); block_it != blocks_data_.rend(); block_it++)
			{
				Cost block_cost = block_it->pair_cost;
				cost_to_block[block_cost] = *block_it;
			}

			for (double x_pr : rev_max_row_dx)
			{
				vector<double> new_cost_to_est(pairs_count_ + 1, 0.0);
				vector<pair<Cost, Cost>> selected_items(pairs_count_ + 1);
				for (Cost block_cost = 0; block_cost <= pairs_count_; block_cost++)
				{
					double block_pr = cost_to_block[block_cost].pr;
					for (Cost accum_cost = 0; accum_cost <= pairs_count_; accum_cost++)
					{
						Cost sum_cost = block_cost + accum_cost;
						if (sum_cost > pairs_count_)
							break;

						double sum_pr = x_pr * block_pr + cost_to_est[accum_cost];

						if (new_cost_to_est[sum_cost] <= sum_pr)
						{
							new_cost_to_est[sum_cost] = sum_pr;
							selected_items[sum_cost] = make_pair(block_cost, accum_cost);
						}
					}
				}

				cost_to_est = new_cost_to_est;

				vector<vector<Block>> new_history(pairs_count_+1);
				for (Cost c = 0; c <= pairs_count_; c++)
				{
					pair<Cost, Cost> item = selected_items[c];
					new_history[c] = history[item.second];
					new_history[c].push_back(cost_to_block[item.first]);
				}
				history = new_history;
			}

			size_t sum_cost = 0;
			double sum_pr = 0.0;
			for (size_t i = 0; i < history.back().size(); i++)
			{
				auto &block = history.back()[i];
				for (auto i : block.p_max_distr)
					cout << i << " ";
				cout << " pr = " << log2(block.pr) << " c2 = " << block.pair_cost << endl;
				sum_cost += block.pair_cost;
				sum_pr += rev_max_row_dx[i] * block.pr;
			}

			cout << "sum_cost = "  << sum_cost << endl;
			cout << "sum_pr = " << log2(sum_pr) << endl;

			return cost_to_est.back();
		}

		double get_est_on_theta_plus_2() const
		{
			using Cost = size_t;

			vector<double> rev_max_row_dx(max_row_x_.rbegin(), max_row_x_.rend());

			map<pair<size_t, size_t>, Block> cost_to_block;//(pairs_count_+1, vector<Block>(triples_count_+1));
			vector<vector<double>> cost_to_est(pairs_count_+1, vector<double>(triples_count_+1, 0.0));

			for (auto block_it = blocks_data_.rbegin(); block_it != blocks_data_.rend(); block_it++)
			{
				pair<size_t, size_t> block_cost = make_pair(block_it->pair_cost, block_it->triple_cost);
				cost_to_block[block_cost] = *block_it;
			}

			size_t progr = 0;

			for (double x_pr : rev_max_row_dx)
			{
				cout << "progr = " << progr++ << endl;
				vector<vector<double>> new_cost_to_est(pairs_count_+1, vector<double>(triples_count_+1, 0.0));

				for (auto block_it = cost_to_block.begin(); block_it != cost_to_block.end(); block_it++)
				{
					size_t block_pair_cost = block_it->first.first;
					size_t block_triple_cost = block_it->first.second;
					double block_pr = block_it->second.pr;

					for (Cost pair_cost = 0; pair_cost <= pairs_count_; pair_cost++) 
					{
						Cost sum_pair_cost = block_pair_cost + pair_cost;
						if (sum_pair_cost > pairs_count_)
							break;

						for (Cost triple_cost = 0; triple_cost <= triples_count_; triple_cost++)
						{
							Cost sum_triple_cost = block_triple_cost + triple_cost;
							if (sum_triple_cost > triples_count_)
								break;

							double sum_pr = x_pr * block_pr + cost_to_est[pair_cost][triple_cost];

							if (new_cost_to_est[sum_pair_cost][sum_triple_cost] <= sum_pr)
							{
								new_cost_to_est[sum_pair_cost][sum_triple_cost] = sum_pr;
							}
						}
					}
				}
				cost_to_est = new_cost_to_est;
			}


			vector<double> theta2blocks(triples_count_+1, 0.0);
			for (Cost t = 0; t <= triples_count_; t++)
			{
				size_t pair_cost = 0;
				for (Cost p = 0; p <= pairs_count_; p++)
				{
					if (cost_to_est[p][t] > theta2blocks[t])
					{
						theta2blocks[t] = cost_to_est[p][t];
						pair_cost = p;
					}
				}
				cout << "t = " << t << " p = " << pair_cost << " pr = " << log2(theta2blocks[t]) << endl;
			}

			vector<double> triple_cost_to_est(triples_count_+1, 0.0);

			for (double x_pr : rev_max_row_dx)
			{
				vector<double> new_triple_cost_to_est(triples_count_ + 1, 0.0);

				for (Cost block_cost = 0; block_cost <= triples_count_; block_cost++)
				{
					double block_pr = theta2blocks[block_cost];
					for (Cost accum_cost = 0; accum_cost <= triples_count_; accum_cost++)
					{
						Cost sum_cost = block_cost + accum_cost;
						if (sum_cost > triples_count_)
							break;

						double sum_pr = x_pr * block_pr + triple_cost_to_est[accum_cost];

						if (new_triple_cost_to_est[sum_cost] <= sum_pr)
						{
							new_triple_cost_to_est[sum_cost] = sum_pr;
						}
					}
				}
				triple_cost_to_est = new_triple_cost_to_est;
			}

			return triple_cost_to_est.back();
		}

	public:
		double get_est(Matrix<double> &table, size_t code_distance, size_t active_sbox_count, bool top_weight_optimization = false)
		{
			//code_distance = Theta
			MDS2RoundEstBasic::init(table, code_distance, active_sbox_count);
			gen_blocks_data();

			cout << "N = " << blocks_data_.size() << endl;
			pairs_count_   = number_of_combinations(block_byte_size_, 2) * pow(p_max_count_,2);
			triples_count_ = number_of_combinations(block_byte_size_, 3) * pow(p_max_count_,3);

			cout << "pairs = " << pairs_count_ << " triples = " << triples_count_ << endl;

			if (code_distance == active_sbox_count)
				return get_est_on_theta();

			if ((code_distance + 1) == active_sbox_count)
			{
                if (top_weight_optimization)				
                    return get_est_on_theta_plus_1_with_top_weights();
                else				
                    return get_est_on_theta_plus_1_pairs_method();
			}

			if (code_distance < active_sbox_count)
			{
				return get_est_on_theta_plus_2();
			}

			return 1.0;
		}

};

#endif /* LSX_DIFF_TRAIL_MDS_2ROUND_DYN_PROG_EST_H_ */

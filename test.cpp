/*
 * CTCrypt 2020
 * An algorithm for bounding non-minimum weight differentials in 2-round LSX-ciphers
 * "Proof-of-work" implementation      
 */


#include "common/std_headers.h"
#include "common/differential.h"
#include "common/linear.h"

#include "mds_2round_dyn_prog_est.h"

#include "ciphers/kuznyechik_data.h"
#include "ciphers/khazad_data.h"



int main(int argc, char *argv[])
{
	cout << "An algorithm for bounding non-minimum weight differentials in 2-round LSX-ciphers" << endl;

	auto kuznyechik_norm_ddt = compute_norm_ddt(KuznyechikData::get_sbox());
	auto kuznyechik_norm_lat = compute_norm_lat(KuznyechikData::get_sbox());
	
    auto khazad_norm_ddt = compute_norm_ddt(KhazadData::get_sbox());
	auto khazad_norm_lat = compute_norm_lat(KhazadData::get_sbox());

	MDS2RoundEstDynProg lsx2r_est;

    auto kuznyechik_medp18 = lsx2r_est.get_est(kuznyechik_norm_ddt, KuznyechikData::code_distance, KuznyechikData::code_distance + 1);	
    auto kuznyechik_medp19 = lsx2r_est.get_est(kuznyechik_norm_ddt, KuznyechikData::code_distance, KuznyechikData::code_distance + 2);  

    auto kuznyechik_melp18 = lsx2r_est.get_est(kuznyechik_norm_lat, KuznyechikData::code_distance, KuznyechikData::code_distance + 1);	
    auto kuznyechik_melp19 = lsx2r_est.get_est(kuznyechik_norm_lat, KuznyechikData::code_distance, KuznyechikData::code_distance + 2);  

    auto khazad_medp10 = lsx2r_est.get_est(khazad_norm_ddt, KhazadData::code_distance, KhazadData::code_distance + 1, true);	
    auto khazad_medp11 = lsx2r_est.get_est(khazad_norm_ddt, KhazadData::code_distance, KhazadData::code_distance + 2);	    

    auto khazad_melp10 = lsx2r_est.get_est(khazad_norm_lat, KhazadData::code_distance, KhazadData::code_distance + 1);	
    auto khazad_melp11 = lsx2r_est.get_est(khazad_norm_lat, KhazadData::code_distance, KhazadData::code_distance + 2);	  

    cout << endl;
    cout << "Kuznyechik:" << endl;
    cout << "log2(MEDP18+) = " << log2(kuznyechik_medp18) << endl;
    cout << "log2(MEDP19+) = " << log2(kuznyechik_medp19) << endl;
    cout << "log2(MELP18+) = " << log2(kuznyechik_melp18) << endl;
    cout << "log2(MELP19+) = " << log2(kuznyechik_melp19) << endl;

    cout << endl;
    cout << "Khazad:" << endl;
    cout << "log2(MEDP10+) = " << log2(khazad_medp10) << endl;
    cout << "log2(MEDP11+) = " << log2(khazad_medp11) << endl;
    cout << "log2(MELP10+) = " << log2(khazad_melp10) << endl;
    cout << "log2(MELP11+) = " << log2(khazad_melp11) << endl;
    
    cout << endl;   
	cout << "exit..." << endl;
	return 0;
}



/*
 * kuznyechik_data.h
 *
 */

#ifndef KUZNYECHIK_KUZNYECHIK_DATA_H_
#define KUZNYECHIK_KUZNYECHIK_DATA_H_

#include "../common/std_headers.h"
#include "../common/arrays.h"

class KuznyechikData
{

	public:
		static const size_t sbox_size = 256;
		static const size_t sbox_bit_size = 8;
		static const size_t block_byte_size = 16;
		static const size_t code_distance = 17;
		static const size_t mds_matrix_size = 16;
		static const size_t irred_pol_coef = 0b111000011; //x^8 + x^7 + x^6 + x + 1
		static const uint8_t sbox[sbox_size];
		static const uint8_t linear_const[block_byte_size];

		static Matrix<uint8_t> get_mds_matrix(bool inv = false);
		static vector<uint8_t> get_sbox();

		static void R_inv_transform(uint8_t *block);
		static void L_inv_transform(uint8_t *block);
		static void R_transform(uint8_t *block);
		static void L_transform(uint8_t *block);
		static uint8_t l_transform(uint8_t *block);
};
#endif /* KUZNYECHIK_KUZNYECHIK_DATA_H_ */
